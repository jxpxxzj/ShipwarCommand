﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipwarCommand.Game
{
    class SceneActor
    {
        public BaseScene Scene { get; set; }
        public void LoadScene(BaseScene scene)
        {
            this.Scene = scene;
            scene.Load();
            Console.WriteLine(string.Format("You are now at {0}.",scene.Name));
            //Console.WriteLine(scene.WelcomeText);
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("Actions:");
            int i = 0;
            foreach(BaseAction s in scene.Actions)
            {
                i++;
                Console.Write(string.Format("{0}.{1} ", i, s.Name));
            }
            Console.WriteLine();
            Console.WriteLine("Scenes:");
            foreach (BaseScene s in scene.ChildScene)
            {
                i++;
                Console.Write(string.Format("{0}.{1} ", i, s.Name));
            }
            Console.WriteLine();
        }
        public void LeaveScene(BaseScene target)
        {
            Scene.Leave();
            LoadScene(target);
        }
        public void NavigateTo(int navindex)
        {
            if (navindex <= Scene.Actions.Count)
            {
                Console.Clear();
                Scene.Actions[navindex - 1].Action();
                LoadScene(this.Scene);
            }
                
            else
            {
                Console.Clear();
                LeaveScene(Scene.ChildScene[navindex - Scene.Actions.Count - 1]);
            }
                
        }
    }
}
