﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Game.Action.Harbor;

namespace ShipwarCommand.Game.Scene
{
    class Harbor : BaseScene
    {
        public Harbor()
        {
            this.Name = "Harbor";
            this.WelcomeText = "";
            this.Actions.Add(new Shop());
            this.Actions.Add(new Activity());
            this.Actions.Add(new Mission());
            this.Actions.Add(new Illustration());
            this.Actions.Add(new Mail());
            this.Actions.Add(new Standings());

        }
        public override void Load()
        {
            base.Load();
        }
        public override void Leave()
        {
            base.Leave();
        }
    }
}
