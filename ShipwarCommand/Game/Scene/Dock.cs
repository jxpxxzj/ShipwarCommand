﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Game.Action.Dock;

namespace ShipwarCommand.Game.Scene
{
    class Dock : BaseScene
    {
        public Dock()
        {
            this.Name = "Dock";
            this.Actions.Add(new Formation());
            this.Actions.Add(new Repair());
            this.Actions.Add(new Supply());
            this.Actions.Add(new Equip());
        }
        public override void Load()
        {
            base.Load();
        }
        public override void Leave()
        {
            base.Leave();
        }
    }
}
