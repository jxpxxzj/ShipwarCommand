﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Game.Action.Procession;

namespace ShipwarCommand.Game.Scene
{
    class Procession : BaseScene
    {
        public Procession()
        {
            this.Name = "Procession";
            this.WelcomeText = "";
            this.Actions.Add(new Building());
            this.Actions.Add(new Disassembly());
            this.Actions.Add(new Developing());
            this.Actions.Add(new Disintegrate());

        }
        public override void Load()
        {
            base.Load();
        }
        public override void Leave()
        {
            base.Leave();
        }
    }
}
