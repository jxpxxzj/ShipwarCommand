﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipwarCommand.Game
{
    class BaseAction
    {
        public string Name { get; set; }
        public virtual void Action()
        {
            Console.Clear();
        }
    }
}
