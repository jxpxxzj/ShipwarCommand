﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipwarCommand.Game
{
    class BaseScene
    {
        public string Name { get; set; }
        public string WelcomeText { get; set; }

        public List<BaseScene> ChildScene = new List<BaseScene>();
        public List<BaseAction> Actions = new List<BaseAction>(); 

        public BaseScene()
        {

        }
        public virtual void Load()
        {
            Console.Clear();
        }
        public virtual void Leave()
        {

        }
        
    }
}
