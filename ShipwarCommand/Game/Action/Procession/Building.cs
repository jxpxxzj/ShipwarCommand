﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Server;

namespace ShipwarCommand.Game.Action.Procession
{
    class Building : BaseAction
    {
        private List<Builder> builder = new List<Builder>();
        public Building()
        {
            this.Name = "Building";
            builder.Add(new Builder(1));
            builder.Add(new Builder(2));
            builder.Add(new Builder(3));
            builder.Add(new Builder(4));
        }
        public override void Action()
        {
            base.Action();
            Console.WriteLine("Builder Status:");
            foreach (Builder b in builder)
            {
                Console.WriteLine(b.ToString());
            }

            Console.WriteLine("Choose the factory, or 0 to exit:");
            int fact = int.Parse(Console.ReadLine());
            if(fact==0)
                return;
            else
            {
                builder[fact - 1].BuildShip();
                Action();
            }
                
        }
    }
    class Builder
    {
        public enum BuildStatus
        {
            Available, Building
        }
        public Builder(int i)
        {
            ID = i;
        }

        public int ID { get; set; }
        public BuildStatus Status { get; set; }
        public DateTime BeginTime { get; set; }
        public override string ToString()
        {
            if (Status == BuildStatus.Available)
                return string.Format("{0}:{1}", ID, Status);
            else
                return string.Format("{0}:{1} ({2})", ID, Status, (BeginTime - DateTime.Now).ToString(@"hh\:mm\:ss"));
        }
        public async void BuildShip()
        {
            if (Status != BuildStatus.Building)
            {
                Console.WriteLine("Input resources,split by space:");
                string input = Console.ReadLine();
                string[] resources = input.Split(' ');

                Random rand = new Random();
                this.Status = BuildStatus.Building;
                Console.Clear();
                int lasttime = rand.Next(100);
                var ship = Server.ServerCommand.GetBuildableRandomShip();
                Console.WriteLine(string.Format("Your ship is building now. Time: {0}", ship.BuildTime.ToString(@"hh\:mm\:ss")));
                BeginTime = DateTime.Now + new TimeSpan(0, 0, lasttime);
                await Task.Delay(lasttime *1000);

                Console.WriteLine("Build successfully.");
                Console.WriteLine(string.Format("Ship:{0} Stars:{1} Type:{2}",ship.Name,ship.Stars,ship.Type));
                Console.WriteLine(ship.GettingTalk);
                this.Status = BuildStatus.Available;
                Console.WriteLine();
            }
            else
            {
                Console.Clear();
                var span = BeginTime - DateTime.Now;
                Console.WriteLine(string.Format("This factory is building now, time left:{0}", span.ToString(@"hh\:mm\:ss")));
                Console.WriteLine();
            }
        }
    }
}
