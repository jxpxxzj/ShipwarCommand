﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Data;

namespace ShipwarCommand.Game.Action.Dock
{
    class Formation : BaseAction
    {
        public Formation()
        {
            this.Name = "Formation";
        }
        public override void Action()
        {
            base.Action();
            int i = 0;
            foreach(Fleet f in User.Fleet)
            {
                i++;
                Console.WriteLine(string.Format("{0}:{1}",i, User.Fleet[i-1].Name));
            }
            
            Console.WriteLine("Input number to see details about the fleet, or input 0 to return.");
            int input = int.Parse(Console.ReadLine());
            if (input == 0)
                return;
            else
                FleetDetail(input);

        }
        private void FleetDetail(int indf)
        {
            Console.Clear();
            int j = 0;
            foreach (BaseShip s in User.Fleet[indf - 1].Ship)
            {
                j++;
                Console.WriteLine(string.Format("Ship {0}:{1}", j, User.Fleet[indf - 1].Ship[j - 1].Name));
            }
            Console.WriteLine("Input number to see details about the ship, or input 0 to return.");
            int input = int.Parse(Console.ReadLine());
            if (input == 0)
                Action();
            else
                ShipDetail(indf,input);          
        }
        private void ChangeShip(int indf,int inds)
        {

        }
        private void ShipDetail(int indf, int inds)
        {
            Console.Clear();
            Console.WriteLine(User.Fleet[indf - 1].Ship[inds - 1].ToString());
            Console.WriteLine("Press Enter to return.");
            Console.ReadLine();
            FleetDetail(indf);
        }
    }
}
