﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Game;
using ShipwarCommand.Game.Scene;
using ShipwarCommand.Data;

using ShipwarCommand.Data.Map;

namespace ShipwarCommand
{
    static class User
    {
        public static List<Fleet> Fleet = new List<Fleet>();
    }
    static class Program
    {
        public static void Main(string[] args)
        {
            var harbor = new Harbor();
            var procession = new Procession();
            var factory = new Factory();
            var transfomation = new Transfomation();
            var dock = new Dock();

            var fleet = new Fleet();
            fleet.Name = "First Fleet";
            fleet.Ship.Add(new Ship.Shengwang());
            User.Fleet.Add(fleet);
            
            harbor.ChildScene.Add(procession);
            harbor.ChildScene.Add(factory);
            harbor.ChildScene.Add(dock);
            harbor.ChildScene.Add(transfomation);

            procession.ChildScene.Add(harbor);
            factory.ChildScene.Add(harbor);
            transfomation.ChildScene.Add(harbor);
            dock.ChildScene.Add(harbor);


            SceneActor sa = new SceneActor();
            sa.Scene = harbor;
            sa.LoadScene(harbor);

            //ShipwarCommand.Server.ServerCommand.GetShipInfo(1);
            //Console.WriteLine(mapjson());

            while (1==1)
            {
                string input = Console.ReadLine();
                if(input!="")
                {
                    if(input!="0")
                    {
                        int target = int.Parse(input);
                        sa.NavigateTo(target);
                    }
                    else
                    {
                        System.Environment.Exit(0);
                    }
                }
                else
                {
                    Console.WriteLine("Input cannot be empty.");
                }
               
            }
        }
        public static string mapjson()
        {
            var begin = new Node("Begin");
            var A = new Node("A"); 
            var B = new Node("B");
            var C = new Node("C");
            var D = new Node("D");
            var E = new Node("E");
            var F = new Node("F");

            begin.OutNode.Add(A);
            begin.OutNode.Add(B);

            A.OutNode.Add(C);

            B.OutNode.Add(C);
            B.OutNode.Add(D);

            C.OutNode.Add(E);

            D.OutNode.Add(F);

            E.OutNode.Add(F);

            var nm = new NodeMap();
            nm.Nodes.Add(begin);
            nm.Nodes.Add(A);
            nm.Nodes.Add(B);
            nm.Nodes.Add(C);
            nm.Nodes.Add(D);
            nm.Nodes.Add(E);
            nm.Nodes.Add(F);

            return Utilities.Serialize.JsonSerialize(nm);

        }
    }
}
