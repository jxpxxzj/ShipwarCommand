﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipwarCommand.Data.Ship;
using ShipwarCommand.Data;
namespace ShipwarCommand.Ship
{
    class Shengwang :BC
    {
        public Shengwang()
        {
            this.Type = ShipType.BC;
            this.Name = "声望";
            this.Stars=3;
            this.Speed=31.5;
            this.Range = FireRange.Long;

            this.FirePower = 58;
            this.Submarine = 0;
            this.Protect = 56;
            this.Air = 52;
            this.Health = 65;
            this.Evade = 30;
            this.Submarine = 0;
            this.Search = 14;
            this.Fortune = 25;
        }
        public override string ToString()
        {
            return Utilities.Serialize.JsonSerialize(this);
        }
    }
}
