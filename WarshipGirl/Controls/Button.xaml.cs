﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WarshipGirl.Controls
{
    /// <summary>
    /// Button.xaml 的交互逻辑
    /// </summary>
    public partial class Button : UserControl
    {
        public Button()
        {
            InitializeComponent();
        }
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("text", typeof(string), typeof(Button), new PropertyMetadata("Text", new PropertyChangedCallback(OnTextChange)));
        public static readonly RoutedEvent ButtonClickEvent = EventManager.RegisterRoutedEvent("", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(Button));
        public event RoutedPropertyChangedEventHandler<object> ButtonClick
        {
            add
            {
                this.AddHandler(ButtonClickEvent,value);
            }
            remove
            {
                this.RemoveHandler(ButtonClickEvent, value);
            }
        }
        protected void OnButtonClick(object oldValue,object newValue)
        {
            RoutedPropertyChangedEventArgs<object> arg =
               new RoutedPropertyChangedEventArgs<object>(oldValue, newValue, ButtonClickEvent);
            this.RaiseEvent(arg);
        }
        protected static void OnTextChange(object sender,DependencyPropertyChangedEventArgs args)
        {
            Button source=(Button)sender;
            source.buttonText.Content=(string)args.NewValue;
        }
        private void baseButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            baseButton.Fill = new ImageBrush(new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btnCommon_2.png")));
        }

        private void baseButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            baseButton.Fill = new ImageBrush(new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btnCommon_1.png")));
            OnButtonClick(new object(), new object());        
        }

        private void buttonText_MouseDown(object sender, MouseButtonEventArgs e)
        {
            baseButton.Fill = new ImageBrush(new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btnCommon_2.png")));
        }

        private void buttonText_MouseUp(object sender, MouseButtonEventArgs e)
        {
            baseButton.Fill = new ImageBrush(new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/btnCommon_1.png")));
            OnButtonClick(new object(), new object());
        }
    }
}
