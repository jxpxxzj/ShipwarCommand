﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarshipGirl.Pages.FactoryPage;
using WarshipGirl.Utilities;

namespace WarshipGirl.Pages
{
    /// <summary>
    /// Factory.xaml 的交互逻辑
    /// </summary>
    public partial class Factory : Page
    {
        public static Building building;
        public static Harbor baseharbor;
        AudioStream str;
        AudioPlayer player;
        public Factory(Harbor basehb)
        {
            InitializeComponent();
            building = new Building(this);
            baseharbor = basehb;
        }

        private void Button_ButtonClick(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ActionFrame.Navigate(building);
        }

        private void Label_MouseUp(object sender, MouseButtonEventArgs e)
        {
            baseharbor.basewindow.ReturnHarbor();
            player.Stop();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            str = new AudioStream(@"Resources/factory.mp3", true);
            player = new AudioPlayer(str);
            player.Play(true);
            ActionFrame.Navigate(building);
        }
    }
}
