﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using WarshipGirl.Utilities;

namespace WarshipGirl.Pages
{
    /// <summary>
    /// Harbor.xaml 的交互逻辑
    /// </summary>
    public partial class Harbor : Page
    {
        public MainWindow basewindow;
        AudioStream str;
        AudioPlayer player;
        public Harbor(MainWindow basewnd)
        {
            InitializeComponent();
            this.basewindow = basewnd;
        }

        private void Button_ButtonClick(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            player.Stop();
            basewindow.Navigate();
        }

        public void ScaleEasingAnimation(FrameworkElement element)
        {
            ScaleTransform scale = new ScaleTransform();
            element.RenderTransform = scale;
            element.RenderTransformOrigin = new Point(0.5, 0.5);
            DoubleAnimation scaleAnimation = new DoubleAnimation()
            {
                From = 1,                                
                To = 1.1,                                   
                Duration = new TimeSpan(0, 0, 10),
                AutoReverse = true,
                RepeatBehavior = RepeatBehavior.Forever

            };
            AnimationClock clock = scaleAnimation.CreateClock();
            scale.ApplyAnimationClock(ScaleTransform.ScaleXProperty, clock);
            scale.ApplyAnimationClock(ScaleTransform.ScaleYProperty, clock);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (basewindow.isNightMode)
            {
                this.bgGrid.Background = new ImageBrush(new BitmapImage(new Uri(@"Resources\dark_harbor.png", UriKind.Relative)));
                str = new Utilities.AudioStream(@"I:\ShipwarCommand\WarshipGirl\bin\Debug\Resources\port-night.mp3", true);
            }
            else
            {
                str = new Utilities.AudioStream(@"I:\ShipwarCommand\WarshipGirl\bin\Debug\Resources\port-day.mp3", true);
            }
            ScaleEasingAnimation(shipImage);
            player = new AudioPlayer(str);
            player.Volume = 100;
            player.Play(true);
        }

        private void Rectangle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            player.Stop();
            basewindow.ToFactory();
        }
    }
}
