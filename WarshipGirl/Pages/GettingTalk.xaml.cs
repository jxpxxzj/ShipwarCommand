﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarshipGirl.Data;
using WarshipGirl.Utilities;

namespace WarshipGirl.Pages
{
    /// <summary>
    /// GettingTalk.xaml 的交互逻辑
    /// </summary>
    public partial class GettingTalk : Page
    {
        public MainWindow baseWindow;
        private BaseShip getship;
        AudioStream stream;
        AudioPlayer player;

        public ImageSource ShipImageSource { get; set; }
        public string ShipName { get; set; }
        public ImageSource ShipStarBG { get; set; }
        public string ShipType { get; set; }
        public string ShipGreeting { get; set; }
        public GettingTalk(MainWindow basewnd,BaseShip ship)
        {
            InitializeComponent();
            this.baseWindow = basewnd;
            getship = ship;
            this.DataContext = ship;
            try
            {
                this.shipImage.Fill = new ImageBrush(new BitmapImage(new Uri(string.Format(@"G:\shipwar\rename\ship1024_normal_{0}.png", ship.ID))));
            }
            catch (Exception)
            {

            }
            finally
            {
                this.starBG.Background = new ImageBrush(new BitmapImage(new Uri(string.Format(@"Resources\fullColor{0}@2x.png", ship.Stars), UriKind.Relative)));
                this.lblShipName.Content = ship.Name;
                this.tbShipGreeting.Text = ship.GettingTalk;
                this.lblShipType.Content = BaseShip.ShipTypeToString(ship.Type);
            }
            
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            stream = new AudioStream(@"Resources\getship.mp3", true);
            player = new AudioPlayer(stream);
            player.Play(true);
        }

        private void Page_MouseDown(object sender, MouseButtonEventArgs e)
        {
            player.Stop();
            baseWindow.ReturnHarbor();
        }
    }
}
