﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarshipGirl.Utilities;
using Un4seen.Bass;
using WarshipGirl.Pages;
using WarshipGirl.Server;

namespace WarshipGirl
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool isNightMode = false;
        public MainWindow()
        {
            InitializeComponent();
            BassNet.Registration("28256042@qq.com", "2X123012150022");
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            harbor = new Harbor(this);
            factory = new Factory(harbor);
            Timeline.DesiredFrameRateProperty.OverrideMetadata(
            typeof(Timeline),
            new FrameworkPropertyMetadata { DefaultValue = 5 }
            );

        }
        public static Harbor harbor;
        public static GettingTalk gettingtalk;
        public static Factory factory;
        public void Navigate()
        {
            gettingtalk = new GettingTalk(this, ServerCommand.GetBuildableRandomShip());
            this.PageContainer.Content = gettingtalk;
        }
        public void ToFactory()
        {
            this.PageContainer.Content = factory;
        }
        public void ReturnHarbor()
        {
            this.PageContainer.Content = harbor;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(isNightMode)
            {
                lblTitle.Background = new SolidColorBrush(Color.FromRgb(45, 45, 48));
                lblTitle.Foreground = new SolidColorBrush(Color.FromRgb(241, 241, 241));

                lblExit.Background = new SolidColorBrush(Color.FromRgb(45, 45, 48));
                lblExit.Foreground = new SolidColorBrush(Color.FromRgb(241, 241, 241));

                lblMin.Background = new SolidColorBrush(Color.FromRgb(45, 45, 48));
                lblMin.Foreground = new SolidColorBrush(Color.FromRgb(241, 241, 241));
            }
            this.PageContainer.Content = harbor;
            
        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Label_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown(0);
        }

        private void Label_MouseMove(object sender, MouseEventArgs e)
        {
            lblExit.Background = new SolidColorBrush(Colors.Red);
            lblExit.Foreground = new SolidColorBrush(Colors.White);
        }

        private void lblExit_MouseLeave(object sender, MouseEventArgs e)
        {
            if (isNightMode)
            {
                lblExit.Background = new SolidColorBrush(Color.FromRgb(45, 45, 48));
                lblExit.Foreground = new SolidColorBrush(Color.FromRgb(241, 241, 241));
            }
            else
            {
                lblExit.Background = new SolidColorBrush(Colors.White);
                lblExit.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void lblMin_MouseMove(object sender, MouseEventArgs e)
        {
            lblMin.Background = new SolidColorBrush(Colors.Blue);
            lblMin.Foreground = new SolidColorBrush(Colors.White);
        }

        private void lblMin_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void lblMin_MouseLeave(object sender, MouseEventArgs e)
        {
            if (isNightMode)
            {
                lblMin.Background = new SolidColorBrush(Color.FromRgb(45, 45, 48));
                lblMin.Foreground = new SolidColorBrush(Color.FromRgb(241, 241, 241));
            }
            else
            {
                lblMin.Background = new SolidColorBrush(Colors.White);
                lblMin.Foreground = new SolidColorBrush(Colors.Black);
            }
        }
    }
}
