﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace DataFinder
{
    public class shipdata
    {
        public int FOCUS;
        //public string GAI_ZAO_INFO;
        public string ZHEN_CHA_MAX;
        public string SHAN_BI;
        public string SKILL;
        public int ID;
        public string DAN;
        public string DUI_KONG;
        public string ATTACK_MAX;
        public string LIFE_MAX;
        public string TYPE;
        //public string LINK_MENGBAI;
        public int CAN_BUILD;
        public string WEDDING_TALK;
        public string NAME;
        public string OIL_COST;
        public string ZHEN_CHA;
        public string GAI_ZAO_USE;
        public string EXP;
        public string SKILL_NAME;
        public int IS_OPENED;
        //public string DROP_INFO;
        public string ATTACK;

        //public MAXDATA MAX_DATA;

        public string YOU;
        //public string IMG_PO;
        public string SKILL_INFO;
        //public string IMG;
        public string LIFE;
        public string DUI_QIAN;
        public string SHAN_BI_MAX;
        public string ITEM_NUM;
        public string TIME_COST;
        public string GETTING_TALK;
        public string PIN_YIN;
        public string SHE_CHENG;
        public string NO;
        public string SU_DU;
        public string DUI_KONG_MAX;
        //public string WEAPON_INFO;
        public string DEFENCE;
        public string GAI_ZAO;
        public string ROCKETS_MAX;
        public string BUILD_TIME;
        public string XING_YUN;
        public string SELL;
        public string LEVEL;
        public string NI_CHENG;
        public string DUI_QIAN_MAX;
        public string STARS;
        public string DA_ZAI;
        public string ROCKETS;
        public string DESC;
        public string GANG_COST;
        public string FEN_BU;
        public string NATION;
        public string WEAPON;
        //public string IMG_HEAD;
        public string DEFENCE_MAX;

        public string ToSQLValue()
        {
            string s = string.Empty;
            s += "values(";
            foreach (FieldInfo info in typeof(shipdata).GetFields())
            {
                s += "'" + info.GetValue(this) + "',";
            }
            s=s.Substring(0, s.Length - 1);
            s += ")";
            return s;
        }
    }
    public class MAXDATA
    {
        public int MAXXINGYUN;
        public int MINATTACK;
        public int MINDEFENCE;
        public int MAXYOU;
        public string MAXSU_DU;
        public int MAXATTACK;
        public int MAXSHANBI;
        public int MAXLIFE;
        public int MAXDAN;
        public int MINDAN;
        public int MINDUIKONG;
        public int MAXDUIQIAN;
        public int MINXINGYUN;
        public string TYPE;
        public int MINDAZAI;
        public int MAXROCKET;
        public int MINROCKET;
        public int MAXDEFENCE;
        public int MINZHENCHA;
        public int MINSHANBI;
        public int MINYOU;
        public int MAXDAZAI;
        public int MINLIFE;
        public int MAXZHENCHA;
        public int MAXDUIKONG;
        public string MINSU_DU;
        public int MINDUIQIAN;
    }
}
